﻿using Moq;
using OTUS.HW.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;

namespace OTUS.HW.InterfacesTest
{
   
    public class AccountManagerTest
    {
        public Mock<IRepository<Account>> IRepository;

        
        [Fact]
        public void AddAccount_EmptyName()
        {
            IRepository =  new Mock<IRepository<Account>>();
            IRepository.Setup(x => x.Add(It.IsAny<Account>()));
            AccountManager accountManager = new AccountManager();
            accountManager.repository = IRepository.Object;

            Account account = new Account()
            {
                FirstName = "",
                LastName = "Smith",
                BirthDate = DateTime.Now
            };

            Action act = () => accountManager.AddAccount(account);
            var exception = Assert.Throws<Exception>(act);
            Assert.Contains("Empty Name", exception.Message);

        }

        [Fact]
        public void AddAccount_EmptyName2()
        {
            IRepository = new Mock<IRepository<Account>>();
            IRepository.Setup(x => x.Add(It.IsAny<Account>()));
            AccountManager accountManager = new AccountManager();
            accountManager.repository = IRepository.Object;

            Account account = new Account()
            {
                FirstName = "Adam",
                LastName = "",
                BirthDate = DateTime.Now
            };

            Action act = () => accountManager.AddAccount(account);
            var exception = Assert.Throws<Exception>(act);
            Assert.Contains("Empty Name", exception.Message);

        }

        [Fact]
        public void AddAccount_ManyYears()
        {
            IRepository = new Mock<IRepository<Account>>();
            IRepository.Setup(x => x.Add(It.IsAny<Account>()));
            AccountManager accountManager = new AccountManager();
            accountManager.repository = IRepository.Object;

            Account account = new Account()
            {
                FirstName = "Adam",
                LastName = "Smith",
                BirthDate = DateTime.Now
            };

            Action act = () => accountManager.AddAccount(account);
            var exception = Assert.Throws<Exception>(act);
            Assert.Contains("Not many years", exception.Message);

        }

        [Fact]
        public void AddAccount_CorrectAccount()
        {
            var storage = new List<Account>();
            IRepository = new Mock<IRepository<Account>>();
            IRepository.Setup(x => x.Add(It.IsAny<Account>())).Callback<Account>(a => storage.Add(a));
            AccountManager accountManager = new AccountManager();
            accountManager.repository = IRepository.Object;

            Account account = new Account()
            {
                FirstName = "Adam",
                LastName = "Smith",
                BirthDate = DateTime.Now.AddYears(-19)
            };

            accountManager.AddAccount(account);
            
            Assert.Single(storage);

        }

    }
}
