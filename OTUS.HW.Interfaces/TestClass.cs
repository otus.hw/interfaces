﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    class TestClass : TestClassSorted, IComparable
    {
        public string Message { get; set; }

        public int Count { get; set; }

        public int CompareTo(object obj)
        {
            TestClass t = obj as TestClass;
            if (t != null)
                return this.Count.CompareTo(t.Count);
            else
                throw new Exception("Невозможно сравнить");
        }
    }
}
