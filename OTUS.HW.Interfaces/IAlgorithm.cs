﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    public interface IAlgorithm
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
    }
}
