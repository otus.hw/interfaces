﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = "<TestClass><Message>Hello two!</Message><Count>2</Count></TestClass>\n<TestClass><Message>Hello one!</Message><Count>1</Count></TestClass>";
            OtusXmlSerializer<TestClass> testClassSerializer = new OtusXmlSerializer<TestClass>();
            OtusStreamReader<TestClass> testClasses = new OtusStreamReader<TestClass>(data, testClassSerializer);
            foreach (var instanceTestClass in testClasses)
            {
                Console.WriteLine($"{instanceTestClass.Message} {instanceTestClass.Count}");
            }

            TestClassSorted testClassSorted = new TestClassSorted();

            Console.WriteLine("Отсортированный массив по полю count");
            var sorted = testClassSorted.Sort(testClasses);

            foreach (var instanceTestClass in sorted)
            {
                Console.WriteLine($"{instanceTestClass.Message} {instanceTestClass.Count}");
            }

            Console.WriteLine();

           var _accounts = new List<Account>()
            {
                new Account{FirstName =  "Adam", LastName = "Smith", BirthDate = DateTime.Now.AddYears(-20)},
                new Account{FirstName = "Eva", LastName = "Brown", BirthDate = DateTime.Now.AddYears(-17)},
                new Account{FirstName = "Tom", LastName = "Gelly", BirthDate = DateTime.Now.AddYears(-21)}
            };

           
            AccountManager accountManager = new AccountManager();
            foreach (var acc in _accounts)
            {
                try 
                {
                    accountManager.AddAccount(acc);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error when adding {acc.FirstName} {acc.LastName}: {ex.Message} "  );
                }
            }
            Console.WriteLine();

           var accounts = accountManager.repository.GetAll();
            foreach (var acc in accounts)
            {
                Console.WriteLine($"{acc.FirstName} {acc.LastName} {acc.BirthDate}" );
            }
            Console.WriteLine();

           var account = accountManager.repository.GetOne(x => x.FirstName == "Tom");
            Console.WriteLine($"One account: {account.FirstName} {account.LastName}");
            
            Console.ReadKey();
        }
    }
}
