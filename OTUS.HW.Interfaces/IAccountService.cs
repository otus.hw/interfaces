﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }
}
