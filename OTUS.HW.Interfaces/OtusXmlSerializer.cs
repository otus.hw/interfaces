﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OTUS.HW.Interfaces
{
    public interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(string data);
    }
    class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings XmlWriterSettings;

        public T Deserialize<T>(string data)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
        .UseAutoFormatting()
        .UseOptimizedNamespaces()
        .EnableImplicitTyping(typeof(T))
        .Create();
            return serializer.Deserialize<T>(data);
        }

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
        .UseAutoFormatting()
        .UseOptimizedNamespaces()
        .EnableImplicitTyping(typeof(T))
        .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }
    }
}
