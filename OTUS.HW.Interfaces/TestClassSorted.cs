﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    public class TestClassSorted : IAlgorithm
    {
        public IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems)
        {
            var SortedItems = from t in notSortedItems // определяем каждый объект из teams как t
                    orderby t  // упорядочиваем по возрастанию
                    select t;
            return SortedItems;
        }
    }
}
