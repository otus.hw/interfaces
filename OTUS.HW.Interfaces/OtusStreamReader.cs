﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly ISerializer<T> _serializer;
        private readonly string _stream;

        public OtusStreamReader(string data, ISerializer<T> serializer)
        {
            _serializer = serializer;
            _stream = data;
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var line in _stream.Split('\n'))
            {
                yield return _serializer.Deserialize<T>(line); 
            }

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
