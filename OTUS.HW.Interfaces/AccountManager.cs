﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    public class AccountManager:  IAccountService
    {
        public IRepository<Account> repository = new Storage<Account>();
        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account.LastName) || string.IsNullOrEmpty(account.FirstName))
                throw new Exception("Empty Name");
            if (account.BirthDate.AddYears(18) > DateTime.Now)
                throw new Exception("Not many years.");
            repository.Add(account);
        }
    }
}
