﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.HW.Interfaces
{
    public class Storage<T> : IRepository<T>
    {
        private List<T> _repository = new List<T>();

        public List<T> Repository { get => _repository; set => _repository = value; }

        public void Add(T item)
        {
            _repository.Add(item);
        }



        public IEnumerable<T> GetAll()
        {
            foreach (var r in Repository)
            {
                yield return r;
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return _repository.FirstOrDefault<T>(predicate);
        }
    }

}
